import {Book} from "../models/Book";
import {Cd} from "../models/Cd";
export class Cd_BookServices {
  Books : Book []= [
    {
      id : 0,
      name : "Book 0",
      isLend : false
    },
    {
      id : 1,
      name : "Book 1",
      isLend : false
    },
    {
      id : 2,
      name : "Book 2",
      isLend : false
    },
    {
      id : 3,
      name : "Book 3",
      isLend : false
    },
    {
      id : 4,
      name : "Book 4",
      isLend : false
    }
  ]
  Cds : Cd[] = [
    {
      id : 0,
      name : "Cd 0",
      isLend : false
    },
    {
      id : 1,
      name : "Cd 1",
      isLend : false
    },
    {
      id : 2,
      name : "Cd 2",
      isLend : false
    },
    {
      id : 3,
      name : "Cd 3",
      isLend : false
    },
    {
      id : 4,
      name : "Cd 4",
      isLend : false
    }
  ]
}

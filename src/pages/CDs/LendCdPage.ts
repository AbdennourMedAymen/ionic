import {Component} from "@angular/core";
import {NavParams, ViewController} from "ionic-angular";
import {Cd} from "../models/Cd";
import {Cd_BookServices} from "../services/cd_book.services";

@Component({
  selector : 'LendCdPage',
  templateUrl : 'LendCdPage.html'
})

export  class LendCdPage{
  index : number;
  cd : Cd;
constructor(private viewCtrl : ViewController, private navParams : NavParams,private service : Cd_BookServices){}
   ngOnInit(){
  this.index = this.navParams.get("index");
  this.cd = this.service.Cds[this.index];
    }
  dismissModal(){
  this.viewCtrl.dismiss();
  }
  LendClick(){
    this.cd.isLend= !this.cd.isLend;
  }
}

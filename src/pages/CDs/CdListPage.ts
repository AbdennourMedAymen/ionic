import {Component} from "@angular/core";
import {Cd} from "../models/Cd";
import {Cd_BookServices} from "../services/cd_book.services";
import {LendBookPage} from "../books/LendBookPage";
import {ModalController, MenuController} from "ionic-angular";
import {LendCdPage} from "./LendCdPage";


@Component({
  selector : 'CdListPage',
  templateUrl : 'CdListPage.html'
})

export  class CdListPage{
  cds : Cd[];
  constructor(public Cd_BookService : Cd_BookServices, private modalCtrl : ModalController, private menuCtrl : MenuController){

  }
  ionViewWillEnter(){
    this.cds=this.Cd_BookService.Cds.slice();
  }
  GotoCd(index : number){
    let modal = this.modalCtrl.create(LendCdPage,{index:index});
    modal.present();
  }
  onToggleMenu(){
    this.menuCtrl.open();
  }
}

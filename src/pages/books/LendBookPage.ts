import {Component} from "@angular/core";
import {NavParams, ViewController} from "ionic-angular";
import {Book} from "../models/Book";
import {Cd_BookServices} from "../services/cd_book.services";


@Component({
  selector : 'LendBookPage',
  templateUrl : 'LendBookPage.html'
})

export  class LendBookPage{
  book : Book;
  index : number;
   constructor(private navParams : NavParams, private service : Cd_BookServices, private viewCtrl : ViewController){}
   ngOnInit(){
     this.index=this.navParams.get("index");
     this.book= this.service.Books[this.index];
    }
  LendClick(){
     this.book.isLend= !this.book.isLend;
  }
  dismissModal(){
     this.viewCtrl.dismiss();
  }
}

import {Component} from "@angular/core";
import {Book} from "../models/Book";
import {Cd_BookServices} from "../services/cd_book.services";
import {MenuController, ModalController} from "ionic-angular";
import {LendBookPage} from "./LendBookPage";


@Component({
  selector : 'BookListPage',
  templateUrl : 'BookListPage.html'
})

export  class BookListPage{
  books : Book[];
  constructor (private Cd_BooksService : Cd_BookServices, private modalCtrl : ModalController,private menuCtrl : MenuController) {
  }
  ionViewWillEnter(){
    this.books = this.Cd_BooksService.Books.slice();
  }
  GotoBook(index : number){
  let modal = this.modalCtrl.create(LendBookPage,{index:index});
  modal.present();
  }
  onToggleMenu(){
    this.menuCtrl.open();
  }
}

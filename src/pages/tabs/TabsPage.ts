import {Component} from "@angular/core";
import {BookListPage} from "../books/BookListPage";
import {CdListPage} from "../CDs/CdListPage";


@Component({
  templateUrl : 'TabsPage.html'
})

export class TabsPage {
  bookListPage = BookListPage;
  cdListPage = CdListPage;
  constructor(){

  }
}

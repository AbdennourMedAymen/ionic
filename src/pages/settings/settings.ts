import {Component} from "@angular/core";
import { MenuController } from "ionic-angular";

@Component({
 selector : 'settings-page',
  templateUrl : 'settings.html'
})

export class SettingsPage {
  constructor(private menuCtrl : MenuController){}
  onToggleMenu(){
    this.menuCtrl.open();
  }
}
